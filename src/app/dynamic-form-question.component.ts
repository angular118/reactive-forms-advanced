import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { InputBase } from './input-base';

@Component({
  selector: 'app-question',
  templateUrl: './dynamic-form-question.component.html'
})
export class DynamicFormQuestionComponent {
  @Input() question!: InputBase<string>;
  @Input() formy!: FormGroup;
  ngOnInit() {
    console.log(this.formy)
    
  }
  get isValid() { return this.formy.controls[this.question.key].valid; }
}
