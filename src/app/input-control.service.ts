import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CreateFormGroups } from './formGroup-builder';

import { InputBase } from './input-base';

@Injectable()
export class InputControlService {
    constructor() { }

    toFormGroup(questions: InputBase<string>[]) {
        const group: any = {};

        questions?.forEach(question => {
            console.log("question", question);
            group[question.key] = this.createForm(question)
            //   question.required ? new FormControl(question.value || '', Validators.required)
            //   : new FormControl(question.value || '');
        });
        return new FormGroup(group);
    }

    private createForm(question: InputBase<string>) {
        if (question.type == 'formGroup') {

            const group: any = {};

            question.options.forEach(option => {
                group[option.key] = new FormControl(option.value || '');
            });
            // console.log(group)
            return new FormGroup(group);

        } else {
            return question.required ? new FormControl(question.value || '', Validators.required)
                : new FormControl(question.value || '');
        }
    }

}
