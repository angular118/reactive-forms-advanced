import { Injectable } from '@angular/core';

import { DropdownQuestion } from './input-dropdown';
import { InputBase } from './input-base';
import { TextboxQuestion } from './input-textbox';
import { Observable, of } from 'rxjs';
import { CheckBoxFormGroup } from './input-checkbox';

@Injectable()
export class InputService {

    // TODO: get from a remote source of question metadata
    getInputs(): Observable<InputBase<string>[]> {

        const questions: InputBase<string>[] = [

            new DropdownQuestion({
                key: 'brave',
                label: 'Bravery Rating',
                options: [
                    { key: 'solid', value: 'Solid' },
                    { key: 'great', value: 'Great' },
                    { key: 'good', value: 'Good' },
                    { key: 'unproven', value: 'Unproven' }
                ],
                order: 2
            }),

            new TextboxQuestion({
                key: 'firstName',
                label: 'second name',
                value: ' ',
                required: true,
                order: 1
            }),

            new TextboxQuestion({
                key: 'emailAddress',
                label: 'Please enter email',
                type: 'email',
                order: 3
            }),
            new CheckBoxFormGroup({
                key: 'takechai',
                label: 'chai types',
                options: [
                    { key: 'masala', value: 'false' },
                    { key: 'adrak', value: 'false' },
                    { key: 'kadak', value: 'false' },
                    { key: 'sasti', value: 'false' }
                ],
                order: 4
            })
        ];

        return of(questions.sort((a, b) => a.order - b.order));
    }
}
