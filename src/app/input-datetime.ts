import { InputBase } from './Input-base';

export class DropdownQuestion extends InputBase<string> {
    override controlType = 'dropdown';
}
