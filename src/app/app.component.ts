

import { FormControl, FormArray, FormGroup, Validators } from '@angular/forms';
// chips imports 
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DataService } from './data.service';
import { City } from './city.model';
import { InputTypesService } from './inputtypes.service';
import { InputService } from './input.service';
import { InputBase } from './input-base';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [InputService]
})
export class AppComponent implements OnInit {

  gotInputData: boolean = false;
  // https://stackoverflow.com/questions/61322905/angular-8-render-child-component-after-data-required-is-fetched-through-http-ca
  // dynamic form here 
  inputs$: Observable<InputBase<string>[]> = new Observable<InputBase<string>[]>();
  inputarray: InputBase<string>[] = [];


  // sample form with basic radio, input, formarray 
  sampleForm: FormGroup

  dropList: string[] = ['Stable', 'Critical', 'Finished']
  private getDefaultValue = (vname: string): string => { return localStorage.getItem(vname) ?? "" }

  // get inputTypeControls() {
  //   return (<FormArray>this.dynamicForm.get('inputtypes_formArray')).controls
  // }



  constructor(private citydata: DataService, private input_types_service: InputTypesService, private inputService: InputService) {
    // dynamic form constructors 
    // this.inputs$ = inputService.getInputs();
    // this.inputs$ = input_types_service.getInputs();
    this.initDynamicForm();

    console.log("this.input_types_service\n\n\n\n\n\n")
    console.log(input_types_service.getInputs())
    // .subscribe(data => console.log(data)))

    console.log("this.inputsservice$\n\n\n\n\n\n")
    console.log(inputService.getInputs())
    // .subscribe(data => console.log(data)))

    // console.log("this.inputs$\n\n\n\n\n\n")
    // console.log(this.inputs$.subscribe(data => console.log(data)))
    // sample form non-dynamic
    this.sampleForm = new FormGroup({
      projectname: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.email]),
      dropdown: new FormControl(this.dropList[0]),
      hobbiesArray: new FormArray([]),
      gender: new FormControl('')

    });

    // chips constructor 
    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => (fruit ? this._filter(fruit) : this.allFruits.slice())),
    );


    // input autocomplete 
    this.citydata.getJSON().subscribe(data => {
      this.options = data.map(r => (r as City).city);
      // console.log("data from City[]");
      // console.log(data);
    });

    // cities 
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filterinput(value)),
    );

    // this.input_types_data.getJSON().subscribe(data => {
    //   data.forEach((input_type_data) => {
    //     const newinputtype = new FormControl(input_type_data.type);
    //     (<FormArray>this.dynamicForm.get('inputtypes_formArray')).push(newinputtype)
    //   }

    //   )

    //   console.log(data)
    // }
    // )
    // this.citydata.getJSONcity().subscribe(data => {
    //   // this.options.push(data);
    //   console.log("data from City");
    //   console.log(data);
    // });
    // this.inputs$ = [];


  }

  ngOnInit() {
    this.sampleForm.patchValue({
      projectname: this.getDefaultValue('projectname'),
      email: this.getDefaultValue('email'),
    })



  }

  initDynamicForm() {
    // this.inputs$ = await this.input_types_service.getInputs();
    this.inputService.getInputs().subscribe(data => {
      (data as InputBase<string>[]).forEach(d => this.inputarray.push(d))
    });
    this.gotInputData = true;
  }

  get hobbiesArray() {
    return this.sampleForm.get('hobbiesArray') as FormArray;
  }
  addHobby() {
    // console.log(this.sampleForm.controls)
    this.hobbiesArray.push(new FormControl('', Validators.required));
  }

  removeInputControl(idx: number) {
    // this.sampleForm.controls. hobbiesArray.removeAt(idx);
  }

  onSave() {
    console.log("inonsave \n")
    let writing = (variablename: string) => {
      localStorage.setItem(variablename, this.sampleForm.controls[variablename].value);
      // console.log(`stored item ${variablename} with value ${this.sampleForm.controls[variablename].value}`)
    }

    writing('projectname')
    writing('email')

  }

  changeList(e: any) {
    this.sampleForm.patchValue({
      dropdown: e.target.value
    }
    )
  }


  onSubmit() {
    console.log(this.sampleForm.value);
    // this.sampleForm.reset();
  }


  // chips start 
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: string[] = ['Lemon'];
  allFruits: string[] = ['Apple', 'Lemon', 'Lime', 'Orange', 'Strawberry'];

  // note : @ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>; -- throws error. following does not 
  @ViewChild('fruitInput')
  fruitInput!: ElementRef<HTMLInputElement>;


  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.fruits.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();

    this.fruitCtrl.setValue(null);
  }

  remove(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFruits.filter(fruit => fruit.toLowerCase().includes(filterValue));
  }

  // chips end 

  // datepicker range start
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });
  // datepicker range ends 


  //cities start 

  myControl = new FormControl();
  options: string[] = []
  filteredOptions: Observable<string[]>;



  private filterinput(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  //cities end 
}
