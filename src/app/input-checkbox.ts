import { InputBase } from './input-base';

export class CheckBoxFormGroup extends InputBase<string> {
    override controlType = 'checkbox';
    override type = 'formGroup'
}
