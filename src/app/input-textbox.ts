import { InputBase } from './input-base';

export class TextboxQuestion extends InputBase<string> {
    override controlType = 'textbox';
}
