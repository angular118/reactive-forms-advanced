import { InputBase } from './input-base';

export class DropdownQuestion extends InputBase<string> {
    override controlType = 'dropdown';
}
