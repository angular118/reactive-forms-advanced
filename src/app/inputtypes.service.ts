import { HttpClient } from '@angular/common/http';
import { Observable, map, of, tap, pipe } from 'rxjs';
import { Injectable } from '@angular/core';
// import { InputType } from './inputtype.model'
import { TextboxQuestion } from './input-textbox';
import { InputBase } from './input-base';
import { CheckBoxFormGroup } from './input-checkbox';
import { DropdownQuestion } from './input-dropdown';
import { InputJson } from './input-json.model';


@Injectable({
    providedIn: 'root'
})
export class InputTypesService {

    private _jsonURL = '../assets/inputtypes.json';
    input_data: InputBase<string>[] = [];


    constructor(private http: HttpClient) {
        this.getJSON().subscribe(inputjson => {
            // console.log("data", data);
            inputjson.data.forEach(async d => {
                // console.log("\n\nd\n\n")
                // console.log(d)
                switch (d.controlType) {

                    case "textbox": this.input_data.push(new TextboxQuestion(d)); console.log("in getjson textbox"); break;
                    case "checkbox": this.input_data.push(new CheckBoxFormGroup(d)); console.log("in getjson checkbox"); break;
                    case "dropdown": this.input_data.push(new DropdownQuestion(d)); console.log("in getjson dropdown"); break;
                    default: console.log("kaboom!\n\n\n\n\n")
                }
            })
            // console.log("this.input_data\n\n\n\n\n")
            // console.log(this.input_data)
        });
    }


    public getInputs(): Observable<InputBase<string>[]> {

        // console.log("this.input_data")
        // console.log(this.input_data)
        return (of(
            this.input_data.sort((a, b) => a.order - b.order)))
    }

    private getJSON(): Observable<InputJson<InputBase<string>[]>> {
        return this.http.get<InputJson<InputBase<string>[]>>(this._jsonURL).pipe(tap(inputjson => {
            console.log(inputjson)
            console.log("finally")
        }))


    }
}