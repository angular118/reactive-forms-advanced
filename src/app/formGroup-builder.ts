import { FormControl, FormGroup, Validators } from '@angular/forms';

import { InputBase } from './input-base';

export class CreateFormGroups {

    public createForm(question: InputBase<string>) {
        if (question.type == 'formGroup') {
            const group: any = {};

            question.options.forEach(option => {
                group[option.key] = new FormControl(option.value || '');
            });
            return new FormGroup(group);

        } else {
            return question.required ? new FormControl(question.value || '', Validators.required)
                : new FormControl(question.value || '');
        }
    }

    constructor() {
        const group: any = {};

    }
}