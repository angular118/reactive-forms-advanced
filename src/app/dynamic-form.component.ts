import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { InputBase } from './input-base';
import { InputControlService } from './input-control.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  providers: [InputControlService]
})
export class DynamicFormComponent implements OnInit {

  @Input() inputs: InputBase<string>[] | null = [];
  form!: FormGroup;

  payLoad = '';
  // get isValid() { return this.form.controls[this.question.key].valid; }
  constructor(private qcs: InputControlService) {

  }

  ngOnInit() {

  }
  ngAfterContentInit() {
    this.form = this.qcs.toFormGroup(this.inputs as InputBase<string>[]);

    console.log("this.form")
    console.log(this.form)
    // console.log(this.form.controls['takechai'])
  }



  onSubmit() {
    this.payLoad = JSON.stringify(this.form.getRawValue());
  }
}
