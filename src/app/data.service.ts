import { HttpClient } from '@angular/common/http';
import { Observable, map, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { City } from './city.model'


@Injectable({
  providedIn: 'root'
})
export class DataService {

  private _jsonURL = '../assets/city.json';

  constructor(private http: HttpClient) {
    this.getJSON().subscribe(data => {
      console.log("IN SERVICE")
      console.log(data);
    });
  }

  public getJSON(): Observable<City[]> {


    return this.http.get<City[]>(this._jsonURL)
    // .pipe(
    // map((r: City[]) => { 
    //   return r.city as string;
    // })
  }
  // .pipe(
  //   map(
  //     (cityjson: City[]) => {
  //       // console.log({ ...cityjson })
  //       // console.log(cityjson
  //       //   //   .map((cityob: City) => {
  //       //   //   // console.log(cityob.city)
  //       //   //   // return cityob
  //       //   //   cityob
  //       //   // })
  //       // )
  //       return cityjson.map((cityob: City) => cityob.city)
  //     }));





  public getJSONcity(): Observable<City> {
    return this.http.get<City>(this._jsonURL)
      .pipe(
        map(
          (cityjson: City) => cityjson)
      );


  }
}


